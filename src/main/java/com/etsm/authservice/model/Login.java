package com.etsm.authservice.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "login")
public class Login {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "user_id", unique = true, nullable = false, updatable = false)
    private Long userId;

    @Column(name = "login", unique = true, nullable = false)
    @NotBlank
    private String login;

    @Column(name = "password")
    private String password;

    @OneToMany(mappedBy = "login", cascade = CascadeType.REMOVE)
    private Set<Session> session = new HashSet<>();

    public Login(Long id, Long userId, String login, String password, Set<Session> session) {
        this.id = id;
        this.userId = userId;
        this.login = login;
        this.password = password;
        this.session = session;
    }

    public Login() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Session> getSession() {
        return session;
    }

    public void setSession(Set<Session> session) {
        this.session = session;
    }
}
