package com.etsm.authservice.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "session")
public class Session {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "login_id", nullable = false, updatable = false )
    private Login login;

    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @Column(name = "token", nullable = false, unique = true)
    private String token;

    public Session() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Session)) return false;
        Session session = (Session) o;
        return Objects.equals(id, session.id) && Objects.equals(login, session.login) && Objects.equals(isActive, session.isActive) && Objects.equals(token, session.token);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, isActive, token);
    }
}
