package com.etsm.authservice.repository;

import com.etsm.authservice.model.Login;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface LoginRepository extends JpaRepository<Login, Long> {
    Boolean existsByLogin(String login);
    Optional<Login> findLoginByUserId(Long userId);
    Optional<Login> findLoginByLogin(String login);
    List<Login> findByUserIdIn(List<Long> ids);
}
