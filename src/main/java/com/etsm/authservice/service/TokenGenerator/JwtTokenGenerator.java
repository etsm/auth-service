package com.etsm.authservice.service.TokenGenerator;

import DTO.Credentials;
import io.jsonwebtoken.*;
import org.springframework.stereotype.Service;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;

@Service
public class JwtTokenGenerator implements TokenGenerator {

    private final static Long TTL = 86400000L;
    private final String SECRET_KEY = "ZXRzbV9zZWNyZXRfa2V5X2Zvcl90b2tlbl9nZW5lcmF0b3I=";


    @Override
    public String generateToken(Credentials credentials) {
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
        long nowMillis = System.currentTimeMillis();
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET_KEY);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
        String login = credentials.getLogin();
        JwtBuilder builder = Jwts.builder().setId(login + "_generate")
                .setIssuedAt(new Date(nowMillis))
                .setSubject(login)
                .setIssuer(login)
                .claim("userId", credentials.getUserId())
                .signWith(signatureAlgorithm, signingKey);
        long expMillis = nowMillis + TTL;
        Date exp = new Date(expMillis);
        builder.setExpiration(exp);

        return builder.compact();
    }

    @Override
    public Claims verify(String token) {
        return Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(SECRET_KEY))
                .parseClaimsJws(token).getBody();
    }
}
