package com.etsm.authservice.service.TokenGenerator;

import DTO.Credentials;
import io.jsonwebtoken.Claims;

import java.util.Map;

public interface TokenGenerator {
    String generateToken(Credentials credentials);
   Claims verify(String token);
}
