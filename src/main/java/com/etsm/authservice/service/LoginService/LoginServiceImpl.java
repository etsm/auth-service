package com.etsm.authservice.service.LoginService;

import DTO.LoginDTO;
import com.etsm.authservice.model.Login;
import com.etsm.authservice.repository.LoginRepository;
import com.etsm.authservice.repository.SessionRepository;
import exceptions.NotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class LoginServiceImpl implements LoginService{
    private final LoginRepository loginRepository;

    public LoginServiceImpl(LoginRepository loginRepository) {
        this.loginRepository = loginRepository;
    }

    @Override
    public Boolean existUser(Long userId) {
        return this.loginRepository.findLoginByUserId(userId).isPresent();
    }

    @Override
    public Boolean isFreeLogin(String login) {
        return !this.loginRepository.existsByLogin(login);
    }

    @Override
    public Long addUser(LoginDTO loginDTO) throws Exception {
        if(loginDTO.getId() == null){
            throw new Exception("User id is needed!");
        }

        if(isFreeLogin(loginDTO.getLogin())){
            Login login = new Login();
            login.setLogin(loginDTO.getLogin());
            login.setPassword(loginDTO.getPassword());
            login.setUserId(loginDTO.getId());
            Login createdLogin = this.loginRepository.saveAndFlush(login);

            return createdLogin.getId();
        }
        throw new Exception("This login already exist");
    }

    @Override
    public Boolean remove(List<Long> ids) {
        List<Login> target = this.loginRepository.findByUserIdIn(ids);
        this.loginRepository.deleteAll(target);
        return true;
    }

    @Override
    public Login getLogin(String login) {
        return this.loginRepository.findLoginByLogin(login).orElseThrow();
    }

    @Override
    public LoginDTO updateLogin(Long id, String newLogin) throws Exception {
        if(newLogin == null){
            throw new Exception("Login can't be null!");
        }

        if(!isFreeLogin(newLogin)){
            throw new Exception("This login already exist");
        }

        Login login = this.loginRepository.findById(id).orElseThrow(NotFoundException::new);
        login.setLogin(newLogin);
        Login updatedLogin = this.loginRepository.saveAndFlush(login);

        return (LoginDTO) LoginDTO.builder()
                .login(updatedLogin.getLogin()).loginId(updatedLogin.getId()).build();
    }

    @Override
    public LoginDTO getLogin(Long userId) throws NotFoundException {
        Login login = this.loginRepository.findLoginByUserId(userId).orElseThrow(NotFoundException::new);

        return (LoginDTO) LoginDTO.builder().login(login.getLogin()).loginId(login.getId()).build();
    }

    @Override
    public void initialize() {
        this.loginRepository.deleteAll();
    }
}
