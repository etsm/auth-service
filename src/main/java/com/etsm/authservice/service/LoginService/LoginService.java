package com.etsm.authservice.service.LoginService;

import DTO.LoginDTO;
import com.etsm.authservice.model.Login;
import exceptions.NotFoundException;

import java.util.List;

public interface LoginService {
    Boolean existUser(Long userId);
    Boolean isFreeLogin(String login);
    Long addUser(LoginDTO loginDTO) throws Exception;
    Boolean remove(List<Long> ids);
    Login getLogin(String login);
    LoginDTO getLogin(Long userId) throws NotFoundException;
    LoginDTO updateLogin(Long id, String newLogin) throws Exception;
    void initialize();
}
