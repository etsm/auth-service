package com.etsm.authservice.service.SessionService;

import DTO.Credentials;
import DTO.LoginDTO;
import com.etsm.authservice.model.Login;
import com.etsm.authservice.model.Session;
import com.etsm.authservice.repository.SessionRepository;
import com.etsm.authservice.service.LoginService.LoginService;
import com.etsm.authservice.service.TokenGenerator.TokenGenerator;
import org.springframework.stereotype.Service;

@Service
public class SessionServiceImpl implements SessionService{
    private final TokenGenerator tokenGenerator;
    private final LoginService loginService;
    private final SessionRepository sessionRepository;

    public SessionServiceImpl(TokenGenerator tokenGenerator, LoginService loginService, SessionRepository sessionRepository) {
        this.tokenGenerator = tokenGenerator;
        this.loginService = loginService;
        this.sessionRepository = sessionRepository;
    }

    @Override
    public Credentials logon(LoginDTO login) throws Exception {
        if(login == null){
            throw new Exception("Pair login and password is not exist");
        }

        String loginWord = login.getLogin();
        String password = login.getPassword();

        if(loginWord == null || password == null || loginWord.equals("") || password.equals("")){
            throw new Exception("Pair login or password is empty");
        }

        Login loginModel = this.loginService.getLogin(login.getLogin());
        if(!loginModel.getPassword().equals(password)){
            throw new Exception("Permissions denied");
        }

        Credentials credentials = new Credentials();
        credentials.setUserId(loginModel.getUserId());
        credentials.setLogin(login.getLogin());
        String token = tokenGenerator.generateToken(credentials);
        Session session = new Session();
        session.setActive(true);
        session.setLogin(loginModel);
        session.setToken(token);

        this.sessionRepository.saveAndFlush(session);
        credentials.setToken(token);

        return credentials;
    }

    @Override
    public Boolean logout(String token) {
        Session session = this.sessionRepository.findByToken(token).orElseThrow();
        session.setActive(false);
        this.sessionRepository.saveAndFlush(session);
        return true;
    }

    @Override
    public void initialize() {
        this.sessionRepository.deleteAll();
    }
}
