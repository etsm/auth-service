package com.etsm.authservice.service.SessionService;

import DTO.Credentials;
import DTO.LoginDTO;

public interface SessionService {
    Credentials logon(LoginDTO login) throws Exception;
    Boolean logout(String token);
    void initialize();
}
