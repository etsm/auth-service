package com.etsm.authservice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/consul")
public class Consul {
    @GetMapping("/ruok")
    public String getAliveStatus(){
        return "imok";
    }
}
