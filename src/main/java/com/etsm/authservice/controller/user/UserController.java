package com.etsm.authservice.controller.user;


import DTO.Credentials;
import DTO.ListIdsDTO;
import DTO.LoginDTO;
import builders.LoginDTOBuilder;
import com.etsm.authservice.repository.LoginRepository;
import com.etsm.authservice.service.LoginService.LoginService;
import com.etsm.authservice.service.SessionService.SessionService;
import com.etsm.authservice.service.TokenGenerator.TokenGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import exceptions.NotFoundException;
import io.jsonwebtoken.Claims;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {
    private final SessionService sessionService;
    private final LoginService loginService;
    private final TokenGenerator tokenGenerator;


    public UserController(SessionService sessionService, LoginService loginService, TokenGenerator tokenGenerator) {
        this.tokenGenerator = tokenGenerator;
        this.sessionService = sessionService;
        this.loginService = loginService;
    }

    @PostMapping("/add")
    public Long addUser(@RequestBody LoginDTO loginDTO) throws Exception {
        return this.loginService.addUser(loginDTO);
    }

    @DeleteMapping("/remove")
    public Boolean remove(@RequestBody ListIdsDTO ids) {
        return this.loginService.remove(ids.getIds());
    }

    @GetMapping("/loginFree/{login}")
    public Boolean isNotExistLogin(@PathVariable String login) {
        return this.loginService.isFreeLogin(login);
    }

    @GetMapping("/existUser/{id}")
    public Boolean isExistUser(@PathVariable String id) {
        return this.loginService.existUser(Long.parseLong(id));
    }

    @PostMapping("/logon")
    public Credentials logon(@RequestBody LoginDTO loginDTO) throws Exception {
        return this.sessionService.logon(loginDTO);
    }

    @PostMapping("/logout")
    public Boolean logout(@RequestBody Credentials credentials) {
        if (credentials == null) {
            return false;
        }
        return this.sessionService.logout(credentials.getToken());
    }

    @PutMapping("/update/{id}/{newLogin}")
    public LoginDTO updateLogin(@PathVariable String id, @PathVariable String newLogin) throws Exception {
        return this.loginService.updateLogin(Long.parseLong(id), newLogin);
    }

    @GetMapping("/ByUserId/{userId}")
    public LoginDTO getLogin(@PathVariable String userId) throws NotFoundException {
        return this.loginService.getLogin(Long.parseLong(userId));
    }

    @GetMapping("/login")
    public LoginDTO getCurrent(@RequestHeader String token) throws NotFoundException {
        Claims claims = tokenGenerator.verify(token);
        Object userId = claims.get("userId");
        String login = (String)claims.get("iss");
        return LoginDTO.builder().id(Long.parseLong(userId.toString())).login(login).build();
    }
}
