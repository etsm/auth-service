package com.etsm.authservice.controller.system;

import com.etsm.authservice.service.LoginService.LoginService;
import com.etsm.authservice.service.SessionService.SessionService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/system")
public class System {

    private final LoginService loginService;
    private final SessionService sessionService;

    public System(LoginService loginService, SessionService sessionService) {
        this.loginService = loginService;
        this.sessionService = sessionService;
    }

    @DeleteMapping("/initialize")
    public void initialize(){
        this.loginService.initialize();
        this.sessionService.initialize();
    }
}
