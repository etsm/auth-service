package com.etsm.authservice.controller.token;

import DTO.Credentials;
import com.etsm.authservice.service.TokenGenerator.TokenGenerator;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/token")
public class Token {

    private final TokenGenerator tokenGenerator;

    public Token(TokenGenerator tokenGenerator) {
        this.tokenGenerator = tokenGenerator;
    }

    @PostMapping("/generate")
    public String getToken(@RequestBody Credentials credentials) {
        return this.tokenGenerator.generateToken(credentials);
    }

    @PostMapping("/verify")
    public Map<String, Object> verifyToken(@RequestBody Credentials credentials) {
        return this.tokenGenerator.verify(credentials.getToken());
    }
}
