#ВАЖНО! Запускать требуется из монорепозитория
FROM openjdk:11

#Создаем папку для библиотеки классов
RUN mkdir /DataTransferObjectLib

#Создаем папку для севиса
RUN mkdir /auth-service

#Копируем библиотеку классов в образ
COPY ./DataTransferObjectLib /DataTransferObjectLib/

#Копируем сервис
COPY ./auth-service /auth-service/

#Устанавливаем рабочую дирректорию
WORKDIR /auth-service

#Собираем проект
RUN  ./gradlew clean && ./gradlew build -x test

#Сразу прокидываем нужный порт
EXPOSE 4200

#Создаем дирректорию для запуска
RUN mkdir /app

#Копируем JAR в дирректорию для запуска
RUN cp /auth-service/build/libs/*.jar /app/auth-service.jar

#запуск сервиса
ENTRYPOINT [\
"java",\
"-XX:+UnlockExperimentalVMOptions",\
"-Djava.security.egd=file:/dev/./urandom",\
"-jar","/app/auth-service.jar",\
"--spring.config.location=file:///auth-service/src/main/resources/production.yml"]